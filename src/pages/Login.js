import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const [isActive, setIsActive] = useState("true")

	function authenticate(e) {

		e.preventDefault();

		setEmail("");
		setPassword("");

		alert(`"${email}" has been verified! Welcome back!`);
	}

	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		<Form onSubmit={(e) => authenticate(e)}>
			<Form.Label style={{ fontSize: '24px' }}>Login</Form.Label>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={(e) => setEmail(e.target.value)}
					required
				/>
			</Form.Group>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={(e) => setPassword(e.target.value)}
					required
				/>
			</Form.Group>
			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>

				: 

				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>

			}
			
		</Form>
	)
}
